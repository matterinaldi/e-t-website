solver fci
----------

Keywords related to solution of the FCI equations

.. note::

   These sections are relevant only if you have specified ``fci`` in the ``method`` section.

Keywords
^^^^^^^^
.. container:: sphinx-custom

   ``energy threshold: [real]``

   Default: :math:`10^{-6}` (or 1.0d-6)

   Energy convergence threshold, as measured with respect to the previous iteration. Optional.

   .. note::
      The solvers will not check for the energy convergence, if the energy threshold is not set.

.. container:: sphinx-custom

   ``residual threshold: [real]``

   Default: :math:`10^{-6}` (or 1.0d-6)

   Threshold of the :math:`L^{2}`-norm of the residual vector of the FCI equation. Optional

.. container:: sphinx-custom

   ``max reduced dimension: [integer]``

   Default: 100

   The maximal dimension of the reduced space of the Davidson procedure. Optional.

.. container:: sphinx-custom

   ``max iterations: [integer]``

   Default: 100

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number.
   Optional.

.. container:: sphinx-custom

   ``start guess: [string]``

   Default: ``single determinant``

   Specifies start guesses for FCI. Optional.

   Valid keyword values are:

   - ``single determinant`` Set a single element of the FCI vector to one for every state.
     For the ground state it corresponds to the HF determinant.
   - ``random`` Gives random starting guess to the coefficient of the FCI vector.

.. container:: sphinx-custom

   ``states: [integer]``

   Default: 1

   Specifies the number of states to calculate. Optional.

.. container:: sphinx-custom

   ``restart``

   Default: false

   If specified, the solver will attempt to restart from a previous calculation. Optional.

.. container:: sphinx-custom

   ``storage: [string]``

   Default: ``disk``

   Selects storage of records for the CI vectors. Optional.

   Valid keyword values are:

   - ``memory`` Stores records in memory.
   - ``disk`` Stores records on file.
