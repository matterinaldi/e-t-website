pcm
----

Keywords specific to the polarizable continuum model enter into the ``pcm`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``input: [string]``
   
   Specifies if the input parameters are given in the :math:`e^{T}` input file 
   or in an external file handled directly by the external library 
   `PCMSolver <https://pcmsolver.readthedocs.io/en/stable/index.html>`_. Required.
   
   Valid keyword values are

   - ``external``
      .. note::
         No further input has to be given to ``pcm`` because the parameters have to be specified in an external *.pcm*-file.
   - ``internal``
   
.. container:: sphinx-custom

   ``solvent: [string]``
   
   Specifies the solvent outside the cavity.
   
   `Valid keyword values <https://pcmsolver.readthedocs.io/en/stable/users/input.html#available-solvents>`_

.. container:: sphinx-custom

   ``tesserae area: [real]``
   
   Default: 0.3
   
   Area of the finite elements (*tesserae*) the surface is constructed from given in square Angstrom.

.. container:: sphinx-custom

   ``solver type: [string]``
   
   Default: ``iefpcm`` 
   
   Selects the `type of solver <https://pcmsolver.readthedocs.io/en/stable/users/input.html#medium-section-keywords>`_ to be used to solve the PCM equation.
   
   Valid keyword values are
   
   - ``iefpcm`` Integral Equation Formalism PCM
   - ``cpcm`` conductor-like PCM

