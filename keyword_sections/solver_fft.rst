solver fft
----------

Keywords related to Fast Fourier transform (FFT) functionality for the time-dependent coupled cluster code. Below you find keywords that are valid in the two sections ``solver fft dipole moment`` and ``solver fft electric field``.

.. note::

   These sections are relevant only if you have specified ``fft dipole moment`` or ``fft electric field`` in the ``cc td`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``initial time: [real]``
   
   Specifies the start of the interval of interest in the time series file to Fourier transform. Required.

.. container:: sphinx-custom

   ``final time: [real]``
   
   Specifies the end of the interval of interest in the time series file to Fourier transform. Required.

.. container:: sphinx-custom

   ``time step: [real]``
   
   Specifies the time step between the data points in the time series file to Fourier transform. Required.

   .. warning::

      This must be equal to :math:`\text{time step} \times \text{steps between output}` in the propagation section of the calculation that generated time series file.
