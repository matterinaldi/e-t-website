multilevel hf
-------------

Keywords specific to multilevel Hartree-Fock enter into the ``multilevel hf`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

	``initial hf optimization``

	Default: ``false``

	Enables an initial optimization of the full density through a standard HF calculation to a low threshold.

.. container:: sphinx-custom

	``initial hf threshold: [real]``

	Default: 
	:math:`1.0\cdot10^{-1}` (or 1.0d-1)

	Threshold for the initial HF optimization. Should only be specified if ``initial hf optimization`` is also specified.
   
.. container:: sphinx-custom

	``print initial hf``

   Default: false

   Enables the printing of the initial HF optimization in the case that ``initial hf optimization`` is given. Optional. 

.. container:: sphinx-custom

	``cholesky virtuals``

	Default: ``false``

	Enable the use of Cholesky decomposition of the virtual AO density to construct the active virtual orbitals. The default is to use projected atomic orbitals.

.. container:: sphinx-custom

	``cholesky threshold: [real]``

	Default:
	:math:`1.0\cdot10^{-2}` or (1.0d-2)

	Threshold for the Cholesky decomposition of the AO density to construct an active space.

.. container:: sphinx-custom

	``project on minimal basis``

	Default: ``false``

	First diagonalization of the AO fock matrix will be performed in a minimal basis.

