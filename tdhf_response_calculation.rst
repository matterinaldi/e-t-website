Time-dependent Hartree-Fock response calculation
================================================
The computation of Hartree-Fock polarizabilities are currently available for restricted HF. A description of how to perform generic HF calculations are given in the ``Hartree-Fock calculation`` tutorial. In addition to the sections that are needed for a ground state HF calculation, the ``tdhf response`` keyword must be specified in the ``do`` section:

.. code-block:: none

   do
      tdhf excited state
   end do

Furthermore, you must add the :ref:`solver tdhf response section <solver-tdhf-es-section>` and specify the frequencies for which you want to compute the polarizabilities, for instance:

.. code-block:: none

   solver tdhf es
      frequencies: {0.0d0, 0.072d0, -0.072d0}
   end solver tdhf es

A minimal working example is:

.. code-block:: none

  system
     name: methanol
     charge: 0
  end system
  
  do
     tdhf response
  end do
  
  memory
     available: 8
  end memory
  
  method
     hf
  end method
  
  solver tdhf response
     frequencies: {0.0d0, 0.072d0, -0.072d0}
  end solver tdhf response
  
  geometry
  basis: cc-pVDZ
  H        0.8475370000      0.0347360000      1.0345270000
  C        0.3504820000      0.0067450000      0.0608350000
  H        0.6350750000      0.8927640000     -0.5200650000
  H        0.6629360000     -0.8933800000     -0.4828330000
  O       -1.0108350000     -0.0082340000      0.3643870000
  H       -1.4851950000     -0.0326310000     -0.4568520000
  end geometry

If successful, you should find the polarizabilities for the desired frequencies at the end of the output file:

.. code-block:: none 

  Determining TDHF polarizabilities
  =================================

  RHF polarizability for frequency  0.0000E+00:
  --------------------------------------------
     << mu_x, mu_x >>:    -16.530473073826
     << mu_x, mu_y >>:     -0.053395729262
     << mu_x, mu_z >>:     -0.696915279065
     << mu_y, mu_x >>:     -0.053395729293
     << mu_y, mu_y >>:    -14.041555340045
     << mu_y, mu_z >>:     -0.042501961050
     << mu_z, mu_x >>:     -0.696915279078
     << mu_z, mu_y >>:     -0.042501961052
     << mu_z, mu_z >>:    -15.580356442772
  --------------------------------------------

  RHF polarizability for frequency  0.7200E-01:
  --------------------------------------------
     << mu_x, mu_x >>:    -16.738269759140
     << mu_x, mu_y >>:     -0.053425576976
     << mu_x, mu_z >>:     -0.709938445749
     << mu_y, mu_x >>:     -0.053425576986
     << mu_y, mu_y >>:    -14.264814625850
     << mu_y, mu_z >>:     -0.042708032494
     << mu_z, mu_x >>:     -0.709938445776
     << mu_z, mu_y >>:     -0.042708032482
     << mu_z, mu_z >>:    -15.804021596120
  --------------------------------------------

  RHF polarizability for frequency -0.7200E-01:
  --------------------------------------------
     << mu_x, mu_x >>:    -16.738269759140
     << mu_x, mu_y >>:     -0.053425576976
     << mu_x, mu_z >>:     -0.709938445749
     << mu_y, mu_x >>:     -0.053425576986
     << mu_y, mu_y >>:    -14.264814625850
     << mu_y, mu_z >>:     -0.042708032494
     << mu_z, mu_x >>:     -0.709938445776
     << mu_z, mu_y >>:     -0.042708032482
     << mu_z, mu_z >>:    -15.804021596120
  --------------------------------------------



Relevant input sections
-----------------------
:ref:`solver tdhf response <solver-tdhf-response-section>`
